#include <iostream>

#include "add.hpp"

int main()
{
    std::cout << add(3, 4) << std::endl;
    return EXIT_SUCCESS;
}