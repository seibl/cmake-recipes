# How to pass information from CMake to your code?

CMake offers a simple pattern replacement feature to modify source files during configuration.
The replacement can be triggered with 
[configure_file](https://cmake.org/cmake/help/latest/command/configure_file.html).
It will take the file specified by the first argument as input, conduct the replacement and 
save it to the second argument.
It is recommended to always specify a location in your build tree as second argument to allow for
multiple builds with the same sources! 
[configure_file](https://cmake.org/cmake/help/latest/command/configure_file.html)
will replace all occurances of `@CMAKE_VARIABLE@` with the corresponding value of the CMake
variable.