# How to add compilation configurations to your project?

## Brief
[Presets](https://cmake.org/cmake/help/latest/manual/cmake-presets.7.html) are able to replace your custom shell scripts that build your application on different platforms with different settings. Since 3.19 CMake supports presets.

## Description
Presets are based on two main files.
 * `CMakePresets.json` holds the project-wide presets that are checked into the repository.
 * `CMakeUserPresets.json` contains developer specific settings and should not be part of the repository.\
        (In fact, you should at it to your `.gitignore` file.)

Presets have a name and specify flags for the configure, build, test, install, etc stage.
To invoke a preset configuration during the configure stage use e.g.
`cmake --preset <ConfigurePreset> -S <SourceDirectory> -B <BuildDirectory>`
or during the build stage use
`cmake --preset <BuildPreset> --build <BuildDirectory>`.

## Example
This recipe contains two configure presets as an example. The development preset can be selected like this,
`cmake --preset dev -S <SourceDirectory> -B <BuildDirectory>`. It uses `g++` as a compiler and enables compiler warnings.
If you build you will see a warning printed. In contrast, the `production` preset does not enable compiler warnings,
but optimization flags. A build with this configuration does not show warnings.