# How to link against "unsupported" libraries?
If neither CMake nor the library author provides support for CMake you have to help yourself.
Idea: Write a script that enables [find_package](https://cmake.org/cmake/help/latest/command/find_package.html) to find
the library and generates a target you can link to.

Useful commands:  
[find_path](https://cmake.org/cmake/help/latest/command/find_path.html)  
[find_library](https://cmake.org/cmake/help/latest/command/find_library.html)  
find_path and find_library will automatically also search the `Packagename_ROOT` folder. This is the 
recommended way to introduce non-standard search locations.  
[find_package_handle_standard_args](https://cmake.org/cmake/help/latest/module/FindPackageHandleStandardArgs.html)

You can define additional directories for find scripts with [CMAKE_MODULE_PATH](https://cmake.org/cmake/help/latest/variable/CMAKE_MODULE_PATH.html).
