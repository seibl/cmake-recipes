find_path(Math_INCLUDE_DIR add.hpp)
find_library(Math_LIBRARY math)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Math DEFAULT_MSG
        Math_INCLUDE_DIR Math_LIBRARY)

if (Math_FOUND)
    set(Math_LIBRARIES ${Math_LIBRARY})
    set(Math_INCLUDE_DIRS ${Math_INCLUDE_DIR})
    if (NOT TARGET Math::Math)
        add_library(Math::Math UNKNOWN IMPORTED)
        set_target_properties(Math::Math PROPERTIES
                IMPORTED_LOCATION "${Math_LIBRARY}")
        target_include_directories(Math::Math INTERFACE "${Math_INCLUDE_DIR}")
    endif ()
endif ()

mark_as_advanced(
        Math_INCLUDE_DIR
        Math_LIBRARY
)