cmake_minimum_required(VERSION 3.20)

project(02_find_package
        LANGUAGES CXX)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

##########################
# EXECUTABLE
##########################
find_package(Math REQUIRED)

add_executable(calculator)
target_sources(calculator PRIVATE app/main.cpp)
target_link_libraries(calculator PRIVATE Math::Math)
