[![Pipeline status](https://gitlab.mpcdf.mpg.de/mpcdf/training/cmake-recipes/badges/main/pipeline.svg)](https://gitlab.mpcdf.mpg.de/mpcdf/training/cmake-recipes/commits/main)

# CMake recipes for direct use

## Recipes overview

All recipes are minimal working examples and you are invited to try them out on your machine.
If you have troubles running them have a look at [.gitlab-ci.yml](.gitlab-ci.yml) to see how
we run them in our CI.

 * [Concept of targets (VERY IMPORTANT!)](00_targets) 
 * [How to link against "supported" libraries?](01_third_party_libraries) 
 * [How to link against "unsupported" libraries?](02_find_package) 
 * [How to collect source files automatically?](03_glob) 
 * [How to create custom targets to build the documentation, etc?](04_custom_targets)
 * [Local and cache variables?](https://cliutils.gitlab.io/modern-cmake/chapters/basics/variables.html)
 * [How to pass information from CMake to your code?](05_configure_file)
 * [How to get the current git hash at configure time?](06_git_configure)
 * [How to get the current git hash at build time?](07_git_build)
 * [How to manage all the tests in your project?](08_ctest)
 * [How to install a project with CMake?](09_install)
 * [How to add compilation configurations to your project?](10_presets)
 * [How can I influence the build process as a user (custom flags, etc)?](11_customization)

## Why CMake?
 * Automatically search for programs, libraries, and header files
 * Multiple out-of-source builds
 * The ability to select optional components at configuration time
 * The ability to easily switch between static and shared builds
 * Automatic generation of file dependencies and support for parallel builds on most platforms
 * Automatic generation of installers for different platforms
 * Cross-platform: Linux, Windows, macOS
 * Support for multiple build systems: Make, Ninja, Visual Studio and many more
 * ...

[Source](https://cmake.org/cmake/help/book/mastering-cmake/chapter/Why%20CMake.html)

## CMake from a user's perspective

### Which version to choose? Which module to load?
**Always use the newest version available!**  
 * CMake binary is backward compatible
 * CMake should be newer than the tools you are using (compiler, libraries, etc)

### How do I use CMake?
```
cmake -S <src_dir> -B <build_dir> <OPTIONS>              #initialize the build
ccmake <build_dir>                                       #configure the build
cmake --build <build_dir> --verbose                      #build it
cmake --install <build_dir> --prefix <install_dir>       #install it
```

Often used options:
```
-DCMAKE_CXX_COMPILER=icpx         #use a specific compiler
-DCMAKE_BUILD_TYPE=Release        #specify the build type (optimization flags)
```

### How to find libraries installed in non-standard locations?
CMake, in addition to all default directories, search in the directory specified by the 
environment variable `LibraryName_ROOT` for the library.

## CMake from a developer's perspective

### Valuable resources
 * https://cliutils.gitlab.io/modern-cmake/
 * https://cmake.org/cmake/help/book/mastering-cmake/
 * https://jeremimucha.com/category/cmake/
 * https://github.com/friendlyanon/cmake-init (C++)
 * https://github.com/fortuno-repos/cookiecutter-fortran-project (Fortran)

### What minimum required version should I choose?
OS support:
 * 3.14: **The bare minimum. Never set less.** (FWIW, this is also what [cmake-init](https://github.com/friendlyanon/cmake-init) uses)
 * 3.16: Ubuntu 20.04.
 * 3.18: Debian 11 (oldstable).
 * 3.19: First to support Apple Silicon.
 * 3.20: CentOS 8/9 (use AppSteam; 3.26 also available).
 * 3.22: Ubuntu 22.04.
 * 3.25: Debian 12 (stable).
 * 3.28: Ubuntu 24.04.
 * latest: pip/conda-forge/homebew/chocolaty

Features:
 * 3.14/3.15: CLI, FindPython updates
 * 3.16: Unity builds / precompiled headers, CUDA meta features
 * 3.17/3.18: Lots more CUDA, metaprogramming
 * 3.21: HIP language support
 * 3.25: compiling HIP for NVIDIA GPUs

Adapted from [Do's and Don'ts (Modern CMake)](https://cliutils.gitlab.io/modern-cmake/chapters/intro/dodonot.html)


