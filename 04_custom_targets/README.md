# How to create custom targets to build the documentation, etc?

[add_custom_target](https://cmake.org/cmake/help/latest/command/add_custom_target.html)
allows you to bundle arbitrary commands into a target. In this example we use this mechanism to 
create a `doc` target to build the Doxygen documentation. We use
[find_program](https://cmake.org/cmake/help/latest/command/find_program.html) to find the doxygen
executable in the system.

Note: Since the command is run in the build folder
you need to copy all required input files to the build folder 
[file(COPY)](https://cmake.org/cmake/help/latest/command/file.html?highlight=file#copy)!