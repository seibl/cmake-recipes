# How to manage all the tests in your project?

[CTest](https://cmake.org/cmake/help/latest/manual/ctest.1.html) is a powerful utility to orchestrate all your tests.
You can register your test executables with CTest and run them via `ctest`. After running all tests it will print 
statistics and inform you which tests returned a non-zero exit code.

Testing in CMake is enabled via `include(CTest)`. The `BUILD_TESTING` CMake variable
controls whether tests are built and executed. After enabling testing you can add tests via 
[add_test](https://cmake.org/cmake/help/latest/command/add_test.html). It can handle CMake executable targets as 
well as direct paths to binaries. Tests can be further specialized via 
[properties](https://cmake.org/cmake/help/latest/manual/cmake-properties.7.html#test-properties). This includes 
tests that are supposed to fail or labels to only run a subset of the tests.

However, CTest is not a unit testing library. If you want library support to write tests have a look at the 
following libraries. 

For C++
- [googletest](https://cliutils.gitlab.io/modern-cmake/chapters/testing/googletest.html)
- [Catch2](https://cliutils.gitlab.io/modern-cmake/chapters/testing/catch.html)