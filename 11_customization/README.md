# How can I influence the build process as a user?

There are situations where you as a user need to influence the build process.
This can be adding custom compiler flags, include directories, linker flags, etc.  
**If you are the library author you should consider using CMake for doing these thing.
The following is mainly intended for the end-user.**

## Automatic flags added by `CMAKE_BUILD_TYPE`

Targets provide modularization and encapsulation. A target
encapsulates everything needed for a certain build job. It also
is some kind of container that allows efficient use as a building 
block.

| CMAKE_BUILD_TYPE | Compiler Flags                           | Linker Flags     |
|------------------|------------------------------------------|------------------|
| Debug            | `-g`                                     |                  |
| Release          | `-O3 -DNDEBUG`                           |                  |
| RelWithDebInfo   | `-O2 -g -DNDEBUG`                        |                  |
| MinSizeRel       | `-Os -DNDEBUG`                           |                  |

There is no default build type! If you do not specify a build types no flags will be added.

## CMake uses common environment variables to modify the compiler and linker commands.
CMake uses environment variables like `CFLAGS`, `CXXFLAGS`, `LDFLAGS`, ... to initialize its internal variables [[SOURCE]](https://cmake.org/cmake/help/latest/variable/CMAKE_LANG_FLAGS.html#variable:CMAKE_%3CLANG%3E_FLAGS).  

| Environment Variable | CMake Variable |
|----------------------|-----------------|
| CFLAGS               | CMAKE_C_FLAGS   |
| CXXFLAGS             | CMAKE_CXX_FLAGS |
| LDFLAGS              | CMAKE_EXE_LINKER_FLAGS |
| ...                  | ...             |

You can either use the environment variables or the CMake variables directly. If both are given the CMake variables will be used.  
**CAUTION:  
The internal variables are set during the first configure call and are cached. Meaning, subsequent changes to the environment variables will not be followed by CMake. To make CMake aware of the changes you can either delete and recreate the build folder or use `--fresh`[[SOURCE]](https://cmake.org/cmake/help/latest/manual/cmake.1.html#cmdoption-cmake-fresh).**

## Example
The following example shows how to add architecture specific optimization flags. **Note, without `CMAKE_BUILD_TYPE` no optimization level is set.**
```
CXXFLAGS="-march=icelake-server" cmake -S . -B build
cmake --build build --verbose
```
or  
```
cmake -S . -B build -DCMAKE_CXX_FLAGS="-march=icelake-server"
cmake --build build --verbose
```