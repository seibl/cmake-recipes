# Linking against third-party libraries that have CMake support

The use of targets makes it very easy to use third-party libraries if they have proper CMake support.
This support is available if either the library author takes care of providing the necessary files 
(MKL, ...) or if the library is so popular that CMake provides support for it (Boost, OpenMP, MPI, ...).

To illustrate this concept this example links against OpenMP and MKL. For this example to work you 
need a recent oneAPI installation and you need to set `MKL_ROOT` to the MKL directory. 
For example, on MPCDF systems you would load the desired version of the `mkl` module,
and afterwards you could execute (in bash) the following line:
```
export MKL_ROOT=$MKL_HOME
```
