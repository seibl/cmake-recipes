#include <iostream>

#include "githash.hpp"

int main()
{
    std::cout << "git hash: " << GIT_HASH << std::endl;
    exit(EXIT_SUCCESS);
}
